require.config({
    deps: ['boot'],

    paths: {
        templates: '../templates',

        jquery: '../bower_components/jquery/jquery',
        underscore: "../bower_components/underscore/underscore",
        backbone: '../bower_components/backbone/backbone',
        text: '../bower_components/requirejs-text/text',
        jqhammer: '../bower_components/hammerjs/dist/jquery.hammer',
        hammer: '../bower_components/backbone.hammer.js/backbone.hammer',
        fastclick: '../bower_components/fastclick/lib/fastclick',
        howler: '../bower_components/howler/howler',
        paper: '../bower_components/paper/dist/paper'
    },

    shim: {
        underscore: {
            exports: '_'
        },

        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },

        hammer: {
            deps: ['jquery', 'jqhammer', 'underscore', 'backbone']
        },

        Playground: {
            deps: ['backbone', 'hammer']
        }

    }
});