define([
], function () {

    var Pg = {};

    _.extend(Pg, Backbone.Events);

    return _.extend(Pg, {
        module: function( options ) {
            return _.extend( { Views: {} }, options );
        }
    });
});