define([
    'Playground',
    'text!templates/fuse.html'
], function( Pg, template ) {

    var Fuse = Pg.module();

    Fuse.Views.Layout = Backbone.View.extend({
        el: '#playground',

        render: function() {
            this.$el.html( template );
        }

    });

    return Fuse;

});