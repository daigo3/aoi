define([
    'Playground',
    'modules/Fuse'
], function( Pg, Fuse ) {

    var Router = Backbone.Router.extend({

        currentView: null,

        routes: {
            '': 'root'
        },

        changeView: function( view ) {
            if( null != this.currentView ) {
                this.currentView.undelegateEvents();
            }

            this.currentView = view;
            this.currentView.render();
        },

        root: function() {
            this.changeView( new Fuse.Views.Layout() );
        }

    });

    return Router;

});