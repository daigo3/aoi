require([
    'Playground',
    'router'
], function ( Pg, Router ) {

    Pg.router = new Router();
    Backbone.history.start();
    
});